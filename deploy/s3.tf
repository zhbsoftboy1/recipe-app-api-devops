resource "aws_s3_bucket" "zzapp_public_files" {
  bucket        = "${local.prefix}-files-zackz"
  acl           = "public-read"
  force_destroy = true
}
